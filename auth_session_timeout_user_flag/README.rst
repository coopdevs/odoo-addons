========================
Set a boolean flag to users to allow/prevent their sessions to expire
========================

This project modifies the [`auth_session_timeout`](https://github.com/OCA/server-auth/tree/12.0/auth_session_timeout) project to
be able to manage which users are affected by the session termination after timeout and which not.

To do so, an `allow_session_termination` flag parameter is added to the `User` form view, so it can be modified from the UI.

Please don't hesitate to suggest any change for this project.

**Table of contents**

.. contents::
   :local:

Installation
============

## Deploying with pip

We used [odoo setup tools](https://pypi.org/project/setuptools-odoo/#packaging-a-single-addon) to generate the pypi files from the odoo manifests. To deploy any packaged module, so that odoo can later install them, you can create a venv with this name (it's git-ignored)

```shell
python -m venv venv
```
And then pip-install them [from pypi](https://pypi.org/user/coopdevs/).

### Example

```shell
pip install odoo12-addon-auth-session-timeout-user-flag==12.0.1.0.0.99.dev1
```
Beware that for word separation, pypi uses dashes `-` and odoo underscores `_`.


Bug Tracker
===========

Bugs are tracked on `GitHub Issues <https://github.com/coopdevs/check-concurrent-update/issues>`_.
In case of trouble, please check there if your issue has already been reported.
If you spotted it first, help us smashing it by providing a detailed and welcomed
`feedback <https://github.com/coopdevs/check-concurrent-update/issues/new?body=module:%20check_concurrent_update%0Aversion:%2012.0%0A%0A**Steps%20to%20reproduce**%0A-%20...%0A%0A**Current%20behavior**%0A%0A**Expected%20behavior**>`_.

Do not contact contributors directly about support or help with technical issues.

Credits
=======

Authors
~~~~~~~

* Coopdevs Treball SCCL

Contributors
~~~~~~~~~~~~

* Som Connexió

Maintainers
~~~~~~~~~~~

This module is part of the `coopdevs/auth_session_timeout_user_flag <https://gitlab.com/coopdevs/odoo-addons/-/tree/master/auth_session_timeout_user_flag>`_ project on GitHub.

You are welcome to contribute.
