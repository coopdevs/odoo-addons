{
    'name': 'Check concurrency update',
    'version': '12.0.1.0.0',
    'depends': [
        'web',
    ],
    'author': 'Coopdevs Treball SCCL',
    'website': 'https://coopdevs.org',
    'category': 'Concurrency management',
    'license': 'AGPL-3',
    'data': [
        'views/web_assets_backend.xml',
    ],
    'demo': [],
}




