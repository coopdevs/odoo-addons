# Copyright 2021-Coopdevs Treball SCCL (<https://coopdevs.org>)
# - Konykon - <tsagari.kon@coopdevs.org>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    'name': 'Project timesheet read permission',
    "version": "12.0.0.0.5",
    'depends': [
        'helpdesk_mgmt_timesheet'
    ],
    "author": "Coopdevs Treball SCCL",
    "category": "Project",
    "website": "https://coopdevs.org",
    "license": "AGPL-3",
    "summary": """
        Allow users without manager access right to track project time
    """,
    "data": [],
    "installable": True,
}
