# Copyright 2022-Coopdevs Treball SCCL (<https://coopdevs.org>)
# - César López Ramírez - <cesar.lopez@coopdevs.org>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Picking Delivery Comments",
    "version": "14.0.1.0.0",
    "depends": ["sale_stock"],
    "author": "Coopdevs Treball SCCL",
    "category": "Hidden",
    "website": "https://coopdevs.org",
    "license": "AGPL-3",
    "summary": """
        Print comments from delivery contact in Delivery Stock Picking
    """,
    "data": ["report/stock_report_deliveryslip.xml"],
    "installable": True,
}
