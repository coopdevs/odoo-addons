# Copyright 2021-Coopdevs Treball SCCL (<https://coopdevs.org>)
# - César López Ramírez - <cesar.lopez@coopdevs.org>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Verbose Name Ticket",
    "version": "12.0.1.0.2",
    "depends": ["helpdesk_mgmt"],
    "author": "Coopdevs Treball SCCL",
    "category": "Accounting & Finance",
    "website": "https://coopdevs.org",
    "license": "AGPL-3",
    "summary": """
        The name printed for each ticket now is built with the number and also the name
    """,
    "data": [],
    "installable": True,
}
