{
    'name': 'Sale Limit Partner Address',
    'summary': "Limit invoice and shipping address to childs of main partner",
    'version': '14.0.0.0.1',
    'depends': ['sale'],
    'author': "Coopdevs Treball SCCL",
    'website': 'https://coopdevs.org',
    'category': "Cooperative management",
    'description': """
    Limit invoice and shipping address to childs of main partner
    """,
    "license": "AGPL-3",
    'data': [
        "views/sale_order_view.xml",
    ]

}
