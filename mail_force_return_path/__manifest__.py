# Copyright 2021-Coopdevs Treball SCCL (<https://coopdevs.org>)
# - César López Ramírez - <cesar.lopez@coopdevs.org>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Mail Force Return Path",
    "version": "12.0.1.0.1",
    "depends": ["mail"],
    "author": "Coopdevs Treball SCCL",
    "category": "Discuss",
    "website": "https://coopdevs.org",
    "license": "AGPL-3",
    "summary": """
        Force mail smtp header Return-Path to be equal to email_from
    """,
    "data": [],
    "installable": True,
}

