# Copyright 2021-Coopdevs Treball SCCL (<https://coopdevs.org>)
# - konykon - <kon.tsagari@coopdevs.org>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Project Tree View",
    "version": "12.0.1.0.0",
    "depends": ["project"],
    "author": "Coopdevs Treball SCCL",
    "category": "Project Management",
    "website": "https://coopdevs.org",
    "license": "AGPL-3",
    "summary": """
        Tree view for projects.
    """,
    "data": ["views/project_tree_view.xml"],
    "installable": True,
}
