This project overwrite the check concurrency strategy to re-activate it.

- Modify the web client to send the needed information to check the concurrent update of a model field.
- Modify the query to check if the field has been updated

Please don't hesitate to suggest any change for this project.
